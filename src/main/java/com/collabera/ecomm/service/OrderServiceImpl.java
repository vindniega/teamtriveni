package com.collabera.ecomm.service;

import com.collabera.ecomm.model.Order;
import com.collabera.ecomm.model.Product;
import com.collabera.ecomm.model.User;
import com.collabera.ecomm.repository.OrderRepository;
import com.collabera.ecomm.repository.ProductRepository;
import com.collabera.ecomm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Boolean order(Long userId, Long productId) {
        User userOrder = userRepository.findById(userId).get();
        Product productOrder = productRepository.findById(productId).get();
        if(productOrder.getQty() == 0) return false;

        Order newOrder = new Order();
        newOrder.setUser(userOrder);
        newOrder.setProduct(productOrder);

        productOrder.getUsers().add(userOrder);
        productOrder.setQty(productOrder.getQty() - 1);

        productRepository.save(productOrder);
        orderRepository.save(newOrder);
        return true;
    }

    @Override
    public Iterable<Order> getOrders() {
        return orderRepository.findAll();
    }

    @Override
    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        orderRepository.deleteAll();
    }

    @Override
    public Stream<Order> getOrdersFromUser(Long id) {
        return Streamable.of(getOrders()).stream()
                .filter(element -> element.getUser().getId() == id);
    }

}

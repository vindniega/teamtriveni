package com.collabera.ecomm.service;

import com.collabera.ecomm.model.Product;

public interface ProductService {

    void createProduct(Product product);
    Iterable<Product> getProducts();
    void updateProduct(Long id, Product product);
    void deleteUser(Long id);
    void delete();
    Product getCertainProduct(Long id);

}

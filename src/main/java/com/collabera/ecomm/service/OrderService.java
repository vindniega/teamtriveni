package com.collabera.ecomm.service;

import com.collabera.ecomm.model.Order;
import org.aspectj.weaver.ast.Or;

import java.util.stream.Stream;

public interface OrderService {

    Boolean order(Long userId, Long productId);
    Iterable<Order> getOrders();
    void deleteOrder(Long id);
    void deleteAll();
    Stream<Order> getOrdersFromUser(Long id);
}

package com.collabera.ecomm.service;

import com.collabera.ecomm.model.User;

public interface UserService {

    Boolean registerUser(User user);
    Iterable<User> getUsers();
    void updateUser(Long id, User user);
    void deleteUser(Long id);
    User login(User user);
    void delete();
    User getUserProfile(Long id);

}

package com.collabera.ecomm.service;

import com.collabera.ecomm.model.User;
import com.collabera.ecomm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Boolean registerUser(User user) {
        if (userRepository.findByUsername(user.getUsername()) == null){
            userRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public void updateUser(Long id, User user) {
        User userToUpdate = userRepository.findById(id).get();
        userToUpdate.setUsername(user.getUsername());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setIsAdmin(user.getIsAdmin());
        userRepository.save(userToUpdate);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User login(User user) {
        return userRepository.findByUsername(user.getUsername());
    }

    @Override
    public void delete() {
        userRepository.deleteAll();
    }

    @Override
    public User getUserProfile(Long id) {
        return userRepository.findById(id).get();
    }


}

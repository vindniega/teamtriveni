package com.collabera.ecomm.service;

import com.collabera.ecomm.model.Product;
import com.collabera.ecomm.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void createProduct(Product product) {
        productRepository.save(product);
    }

    @Override
    public Iterable<Product> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public void updateProduct(Long id, Product product) {
        Product productToUpdate = productRepository.findById(id).get();
        productToUpdate.setName(product.getName());
        productToUpdate.setType(product.getType());
        productToUpdate.setDescription(product.getType());
        productToUpdate.setPrice(product.getPrice());
        productToUpdate.setImageLink(product.getImageLink());
        productToUpdate.setQty(product.getQty());
        productRepository.save(productToUpdate);
    }

    @Override
    public void deleteUser(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public void delete() {
        productRepository.deleteAll();
    }

    @Override
    public Product getCertainProduct(Long id) {
        return productRepository.findById(id).get();
    }


}

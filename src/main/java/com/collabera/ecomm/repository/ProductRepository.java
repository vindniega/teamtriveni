package com.collabera.ecomm.repository;

import com.collabera.ecomm.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}

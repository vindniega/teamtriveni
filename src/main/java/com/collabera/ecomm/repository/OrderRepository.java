package com.collabera.ecomm.repository;

import com.collabera.ecomm.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}

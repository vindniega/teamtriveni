package com.collabera.ecomm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String type;

    @Lob
    @Column(length = 1000)
    private String description;

    @Column
    private Integer qty;

    @Column
    private Double price;

    @Column
    private String imageLink;

    @ManyToMany(targetEntity = User.class, cascade = CascadeType.ALL)
    private List<User> users;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<Order> orders;

}

package com.collabera.ecomm.controller;

import com.collabera.ecomm.model.User;
import com.collabera.ecomm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public ResponseEntity<Object> userRegister(@RequestBody User user){
        return userService.registerUser(user) ? new ResponseEntity<>("Created!", HttpStatus.OK)
                : new ResponseEntity<>("Username exists!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/")
    public ResponseEntity<Object> usersGet(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> userUpdate(@PathVariable Long id, @RequestBody User user){
        userService.updateUser(id, user);
        return new ResponseEntity<>("Updated!", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> userDelete(@PathVariable Long id){
        userService.deleteUser(id);
        return new ResponseEntity<>("Deleted!", HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> userLogin(@RequestBody User user){
        User userToLogin = userService.login(user);
        return new ResponseEntity<>(userToLogin.getId(), HttpStatus.OK);
    }

    @DeleteMapping("/")
    public ResponseEntity<Object> allDelete(){
        userService.delete();
        return new ResponseEntity<>("Deleted all", HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getUserProfile(@PathVariable Long id){
        return new ResponseEntity<>(userService.getUserProfile(id), HttpStatus.OK);
    }


}

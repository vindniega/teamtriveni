package com.collabera.ecomm.controller;

import com.collabera.ecomm.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
@CrossOrigin("*")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/{userId}/{productId}")
    public ResponseEntity<Object> createOrder(@PathVariable Long userId, @PathVariable Long productId){
        return orderService.order(userId, productId) ? new ResponseEntity<>("Order successful", HttpStatus.OK)
                :  new ResponseEntity<>("No stocks available", HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Object> ordersGet(){
        return new ResponseEntity<>(orderService.getOrders(), HttpStatus.OK);
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<Object> orderDelete(@PathVariable Long orderId){
        orderService.deleteOrder(orderId);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @DeleteMapping("/")
    public ResponseEntity<Object> delete(){
        orderService.deleteAll();
        return new ResponseEntity<>("Deleted All", HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> userOrder(@PathVariable Long id){
        return new ResponseEntity<>(orderService.getOrdersFromUser(id), HttpStatus.OK);
    }
}

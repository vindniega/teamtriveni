package com.collabera.ecomm.controller;

import com.collabera.ecomm.model.Product;
import com.collabera.ecomm.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:3000")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/")
    public ResponseEntity<Object> productCreate(@RequestBody Product product){
        productService.createProduct(product);
        return new ResponseEntity<>("Created!", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<Object> productsGet(){
        return new ResponseEntity<>(productService.getProducts(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> productGet(@PathVariable Long id){
        return new ResponseEntity<>(productService.getCertainProduct(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> productUpdate(@PathVariable Long id, @RequestBody Product product){
        productService.updateProduct(id, product);
        return new ResponseEntity<>("Updated", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> userDelete(@PathVariable Long id){
        productService.deleteUser(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @DeleteMapping("/")
    public ResponseEntity<Object> allDelete(){
        productService.delete();
        return new ResponseEntity<>("Deleted all", HttpStatus.OK);
    }
}
